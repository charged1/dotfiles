#!/bin/sh


/home/sougato/.config/display-setup.sh &
flameshot &
conky -c .config/conky/config.conf &
blueman-applet &
picom &
volumeicon &
nm-applet &
nitrogen --restore &
#mpv --no-video ~/Music/startup.mp3 &
/usr/bin/emacs --daemon &

# XDG autostarts
lxsession &

