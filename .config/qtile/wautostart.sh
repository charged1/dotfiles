#!/bin/sh

flameshot &
conky -c .config/conky/config.conf &
#blueman alternative needed
pasystray &
nm-applet &
swaybg -i dtwp/wallpapers/0225.jpg
emacs --daemon &

way-displays &
way-displays -s DISABLED eDP-1 &
way-displays -s MODE HDMI-A-1 1920 1080 119.98 &
