;; Remove startup message
(setq inhibit-startup-message t)

;; Remove menus
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 10)

(menu-bar-mode -1)

;; Blinking cursor
(blink-cursor-mode 0)

;; Blinking cursor
(blink-cursor-mode 0)

;; Font
(set-face-attribute 'default nil :font "JetBrainsMono Nerd Font" :height 170)
(set-face-attribute 'variable-pitch nil :font "Ubuntu" :height 140 :weight 'regular)
(set-face-attribute 'fixed-pitch nil :font "JetBrainsMono Nerd Font" :height 140)

;; Initialize package sources
(require 'package)

;; MELPA
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;; use-package
(require 'use-package)
(setq use-package-always-ensure t)

;; Ivy
(use-package ivy
  :diminish
  :config
  (ivy-mode 1))

;; Icon pack
(use-package all-the-icons)

;; Nice modeline
(use-package doom-modeline)
(doom-modeline-mode 1)
;;  :custom ((doom-modeline-height 30)))

(custom-set-faces
  '(mode-line ((t (:family "Ubuntu Mono" :height 0.95))))
  ;; '(mode-line-active ((t (:family "Noto Sans" :height 0.9)))) ; For 29+
  '(mode-line-inactive ((t (:family "Ubuntu Mono" :height 0.9)))))

;; Show bracket colours
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Which-key
(use-package which-key
  :defer 0
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 1))

;; Theme package
(use-package doom-themes)

;; Load theme, "t" to stop asking
(load-theme 'doom-dracula t)

;; Counsel
(use-package counsel)
(counsel-mode 1)

;; Vim-like keybinds
(use-package evil
  :init      ;; tweak evil's configuration before loading it
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (evil-mode))
(use-package evil-collection
  :after evil
  :config
  (setq evil-collection-mode-list '(dashboard dired ibuffer))
  (evil-collection-init))
(use-package evil-tutor)

;; Zoom in, out.
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

(use-package beacon)
(beacon-mode 1)

(use-package good-scroll)
(good-scroll-mode 1)

;; Line numbers
(column-number-mode)
(global-display-line-numbers-mode t)

(dolist (mode '(vterm-mode-hook
                term-mode-hook
                dashboard-mode-hook
                shell-mode-hook
                treemacs-mode-hook
                neotree-mode-hook
                ibuffer-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Keybinds
(use-package general
  :config
  (general-evil-setup t))
(nvmap :keymaps 'override :prefix "SPC"
       "SPC"   '(counsel-M-x :which-key "M-x")
       "c c"   '(compile :which-key "Compile")
       "c C"   '(recompile :which-key "Recompile")
       "h r r" '((lambda () (interactive) (load-file "~/.config/emacs/init.el")) :which-key "Reload emacs config")
       "h t"   '(load-theme :which-key "Load theme")
       "t t"   '(toggle-truncate-lines :which-key "Toggle truncate lines")
       "b k"   '(kill-current-buffer :which-key "Kill the current buffer.")
       "b i"   '(ibuffer :which-key "Open iBuffer")
       "o T"   '(vterm :which-key "Open vterm")
       "o b"   '(eaf-open-browser :which-key "Open EAF browser.")
       "b m"   '(ibuffer-filter-by-mode :which-key "Open ibuffer by mode.")
       "."     '(find-file :which-key "Find file"))
(nvmap :keymaps 'override :prefix "SPC"
       "m *"   '(org-ctrl-c-star :which-key "Org-ctrl-c-star")
       "m +"   '(org-ctrl-c-minus :which-key "Org-ctrl-c-minus")
       "m ."   '(counsel-org-goto :which-key "Counsel org goto")
       "m e"   '(org-export-dispatch :which-key "Org export dispatch")
       "m f"   '(org-footnote-new :which-key "Org footnote new")
       "m h"   '(org-toggle-heading :which-key "Org toggle heading")
       "m i"   '(org-toggle-item :which-key "Org toggle item")
       "m n"   '(org-store-link :which-key "Org store link")
       "m o"   '(org-set-property :which-key "Org set property")
       "m t"   '(org-todo :which-key "Org todo")
       "m I"   '(org-toggle-inline-images :which-key "Org toggle inline imager")
       "m T"   '(org-todo-list :which-key "Org todo list")
       "o t"   '(org-babel-tangle :which-key "Org babel tangle")
       "o a"   '(org-agenda :which-key "Org agenda")
       "o d"   '(org-deadline :which-key "Org deadline")
       "s r"   '(split-window-right :which-key "Split Window Right"))

;; Org bullets
(use-package org-bullets
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

;; Bigger next on subheadings
(custom-set-faces
  '(org-level-1 ((t (:inherit outline-1 :height 1.5))))
  '(org-level-2 ((t (:inherit outline-2 :height 1.4))))
  '(org-level-3 ((t (:inherit outline-3 :height 1.3))))
  '(org-level-4 ((t (:inherit outline-4 :height 1.25))))
  '(org-level-5 ((t (:inherit outline-5 :height 1.2))))
)

;; Ensure that anything that should be fixed-pitch in Org files appears that way
(set-face-attribute 'org-block nil    :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-table nil    :inherit 'fixed-pitch)
(set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
(set-face-attribute 'org-code nil     :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-table nil    :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-checkbox nil  :inherit 'fixed-pitch)
(set-face-attribute 'line-number nil :inherit 'fixed-pitch)
(set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch)

(dolist (ah '(org-mode-hook))
  (add-hook ah (lambda () (variable-pitch-mode 1))))

(use-package toc-org
  :commands toc-org-enable
  :init (add-hook 'org-mode-hook 'toc-org-enable))

;; Enable
(toc-org-mode 1)

;; Indent on subheadings
(org-indent-mode 1)

;; Stop indenting when new line is made in org src blocks
(setq org-src-preserve-indentation t)

;; Close stuff
(electric-pair-mode 1)

(use-package vterm)

(use-package python-mode)
(use-package lua-mode)
(use-package markdown-mode)

(use-package dashboard
  :init      ;; tweak dashboard config before loading it
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "Emacs Is More Than A Text Editor!")
  ;;(setq dashboard-startup-banner 'logo) ;; use standard emacs logo as banner
  (setq dashboard-startup-banner "~/.config/emacs/emacs-dash.svg")  ;; use custom image as banner
  (setq dashboard-center-content nil) ;; set to 't' for centered content
  (setq dashboard-items '((recents . 5)
                          (agenda . 5 )
                          (bookmarks . 3)
                          (registers . 3)))
  :config
  (dashboard-setup-startup-hook)
  (dashboard-modify-heading-icons '((recents . "file-text")
			      (bookmarks . "book"))))

(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))

(use-package emojify
  :hook (after-init . global-emojify-mode))

(setq erc-prompt (lambda () (concat "[" (buffer-name) "]"))
      erc-server "irc.libera.chat"
      erc-nick "Charged[m]"
      erc-user-full-name "Charged"
      erc-track-shorten-start 24
      erc-autojoin-channels-alist '(("irc.libera.chat" "#archlinux" "#linux" "#emacs" "#awesome" "freetech studios"))
      erc-kill-buffer-on-part t
      erc-fill-column 100
      erc-fill-function 'erc-fill-static
      erc-fill-static-center 20
      ;; erc-auto-query 'bury
      )

(defcustom neo-window-width 25
  "*Specifies the width of the NeoTree window."
  :type 'integer
  :group 'neotree)

(use-package neotree
  :config
  (setq neo-smart-open t
        neo-window-width 30
        neo-theme (if (display-graphic-p) 'icons 'arrow)
        ;;neo-window-fixed-size nil
        inhibit-compacting-font-caches t
        projectile-switch-project-action 'neotree-projectile-action) 
        ;; truncate long file names in neotree
        (add-hook 'neo-after-create-hook
           #'(lambda (_)
               (with-current-buffer (get-buffer neo-buffer-name)
                 (setq truncate-lines t)
                 (setq word-wrap nil)
                 (make-local-variable 'auto-hscroll-mode)
                 (setq auto-hscroll-mode nil)))))

;; show hidden files
(setq-default neo-show-hidden-files t)

(nvmap :prefix "SPC"
       "t n"   '(neotree-toggle :which-key "Toggle neotree file viewer")
       "d n"   '(neotree-dir :which-key "Open directory in neotree"))

;; (add-to-list 'load-path "~/.emacs.d/site-lisp/emacs-application-framework/")
;; (require 'eaf)

;; (require 'eaf-browser)
;; (require 'eaf-file-manager)
;; (require 'eaf-music-player)
;; (require 'eaf-markdown-previewer)
;; (require 'eaf-pdf-viewer)

;;(use-package ement)

;; (global-unset-key (kbd "<left>"))
;; ;;(global-unset-key (kbd "<right>"))
;; (global-unset-key (kbd "<up>"))
;; (global-unset-key (kbd "<down>"))
;; (global-unset-key (kbd "<C-left>"))
;; ;; (global-unset-key (kbd "<C-right>"))
;; (global-unset-key (kbd "<C-up>"))
;; (global-unset-key (kbd "<C-down>"))
;; (global-unset-key (kbd "<M-left>"))
;; ;; (global-unset-key (kbd "<M-right>"))
;; (global-unset-key (kbd "<M-up>"))
;; (global-unset-key (kbd "<M-down>"))

(use-package company)
(company-mode 1)

;; Using garbage magic hack.
 (use-package gcmh
   :config
   (gcmh-mode 1))
;; Setting garbage collection threshold
(setq gc-cons-threshold 402653184
      gc-cons-percentage 0.6)

;; Profile emacs startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;; Silence compiler warnings as they can be pretty disruptive (setq comp-async-report-warnings-errors nil)

;; Silence compiler warnings as they can be pretty disruptive
(if (boundp 'comp-deferred-compilation)
    (setq comp-deferred-compilation nil)
    (setq native-comp-deferred-compilation nil))
;; In noninteractive sessions, prioritize non-byte-compiled source files to
;; prevent the use of stale byte-code. Otherwise, it saves us a little IO time
;; to skip the mtime checks on every *.elc file.
(setq load-prefer-newer noninteractive)

;; thanks to this article for the help: https://emacs.stackexchange.com/questions/16464/emacs-server-init-when-called-without-file/16485#16485

(defun my-frame-behaviours (&optional frame)
  "Set font settings for all frames, including new ones."
  (with-selected-frame (or frame (selected-frame))
    (set-face-attribute 'default nil :font "JetBrainsMono Nerd Font" :height 170)
    (set-face-attribute 'variable-pitch nil :font "Ubuntu" :height 140 :weight 'regular)
    (set-face-attribute 'fixed-pitch nil :font "JetBrainsMono Nerd Font" :height 140)))
;; Apply immediately for non-daemon emacs
(my-frame-behaviours)
;; Apply for new frames in daemon mode
(add-hook 'after-make-frame-functions 'my-frame-behaviours)

;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))
