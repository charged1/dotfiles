;; -*- no-byte-compile: t; lexical-binding: nil -*-
(define-package "good-scroll" "20211101.942"
  "Good pixel line scrolling."
  '((emacs "27.1"))
  :url "https://github.com/io12/good-scroll.el"
  :commit "a7ffd5c0e5935cebd545a0570f64949077f71ee3"
  :revdesc "a7ffd5c0e593"
  :authors '(("Benjamin Levy" . "blevy@protonmail.com"))
  :maintainers '(("Benjamin Levy" . "blevy@protonmail.com")))
