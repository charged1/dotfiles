#+TITLE: Hyprland Configuration
#+AUTHOR: Charged
#+PROPERTY: header-args :tangle hyprland.conf
#+STARTUP: showeverything
#+auto_tangle: t

* Table of contents :toc:
- [[#about-hyprland][About Hyprland]]
- [[#display][Display]]
  - [[#monitors][Monitors]]
  - [[#nvidia][NVIDIA]]
- [[#programs][Programs]]
  - [[#environment-variables][Environment Variables]]
- [[#autostart][Autostart]]
- [[#look--feel][Look & Feel]]
  - [[#gaps--borders][Gaps & Borders]]
  - [[#rounded-corners-shadows--blur][Rounded Corners, Shadows & Blur]]
  - [[#animations][Animations]]
- [[#layouts][Layouts]]
- [[#keyboard][Keyboard]]
  - [[#input-management][Input Management]]
  - [[#keybindings][Keybindings]]
- [[#windows--workspaces][Windows & Workspaces]]

* About Hyprland
#+CAPTION: Hyprland Scrot
#+ATTR_HTML: :alt Hyprland Scrot :title Hyprland Scrot :align left
[[https://gitlab.com/charged1/dotfiles/-/raw/master/.screenshots/hyprland-terminal.png]]

Hyprland is a Wayland continuation of the old X11 window manager Hypr.
It aims to have flashy animations with effects and configurability options.

Right now I'm just trying to make sure everything works, especially with this NVIDIA nonsense.

I am using waybar alongside this, that config is also on this repository.

* Display
Here I configure my monitors and display options built right into Hyprland!

** Monitors
I disable the laptop monitor and set the main monitor refresh rate to the highest it supports (119.98)
#+begin_src conf
# See https://wiki.hyprland.org/Configuring/Monitors/
monitor = eDP-1, disable
monitor = HDMI-A-1, highrr, 1920x0, 1
#+end_src

** NVIDIA
The documentation is a great tool to learn how to setup Hyprland on NVIDIA systems, here is my configuration with an Optimus system.
#+begin_src conf
env = LIBVA_DRIVER_NAME,nvidia
env = __GLX_VENDOR_LIBRARY_NAME,nvidia
env = NVD_BACKEND,direct

cursor {
    no_hardware_cursors = true
}
#+end_src

* Programs
Setting variables for programs I use, to make it easier later in the config.
#+begin_src conf
# See https://wiki.hyprland.org/Configuring/Keywords/
# Set programs that you use
$terminal = alacritty
$fileManager = thunar
$menu = rofi -show drun
$menur = rofi -show run
#+end_src

** Environment Variables
Defaults
#+begin_src conf
env = XCURSOR_SIZE,24
env = HYPRCURSOR_SIZE,24
#+end_src

* Autostart
Exec-once to not rerun it on refresh
#+begin_src conf
exec-once = waybar &
exec-once = hyprpaper &
exec-once = nm-applet &
exec-once = emacs --daemon &
#+end_src

* Look & Feel
The best part of Hyprland! Mostly default with colour changes.

** Gaps & Borders
Slightly modified with colour changes.
#+begin_src conf
general {
    gaps_in = 5
    gaps_out = 15

    border_size = 2

    # https://wiki.hyprland.org/Configuring/Variables/#variable-types for info about colors
    col.active_border = rgba(33ccffee) rgba(800080ee) 45deg
    col.inactive_border = rgba(595959aa)

    # Set to true enable resizing windows by clicking and dragging on borders and gaps
    resize_on_border = false

    # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
    allow_tearing = false

    layout = dwindle
}
#+end_src

** Rounded Corners, Shadows & Blur
What  I have handled previously on X11 with picom.
#+begin_src conf
decoration {
    rounding = 8

    # Change transparency of focused and unfocused windows
    active_opacity = 1.0
    inactive_opacity = 1.0

    shadow {
        enabled = true
        range = 4
        render_power = 3
        color = rgba(1a1a1aee)
    }

    blur {
        enabled = true
        size = 3
        passes = 1

        vibrancy = 0.1696
    }
}
#+end_src

** Animations
Fun, and also previously picom handled.
#+begin_src conf
animations {
    enabled = yes, please :)

    # Default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = easeOutQuint,0.23,1,0.32,1
    bezier = easeInOutCubic,0.65,0.05,0.36,1
    bezier = linear,0,0,1,1
    bezier = almostLinear,0.5,0.5,0.75,1.0
    bezier = quick,0.15,0,0.1,1

    animation = global, 1, 10, default
    animation = border, 1, 5.39, easeOutQuint
    animation = windows, 1, 4.79, easeOutQuint
    animation = windowsIn, 1, 4.1, easeOutQuint, popin 87%
    animation = windowsOut, 1, 1.49, linear, popin 87%
    animation = fadeIn, 1, 1.73, almostLinear
    animation = fadeOut, 1, 1.46, almostLinear
    animation = fade, 1, 3.03, quick
    animation = layers, 1, 3.81, easeOutQuint
    animation = layersIn, 1, 4, easeOutQuint, fade
    animation = layersOut, 1, 1.5, linear, fade
    animation = fadeLayersIn, 1, 1.79, almostLinear
    animation = fadeLayersOut, 1, 1.39, almostLinear
    animation = workspaces, 1, 1.94, almostLinear, fade
    animation = workspacesIn, 1, 1.21, almostLinear, fade
    animation = workspacesOut, 1, 1.94, almostLinear, fade
}
#+end_src

* Layouts
2 given, one misc.
#+begin_src conf
dwindle {
    pseudotile = true # Master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = true # You probably want this
}

# See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
master {
    new_status = master
}

misc {
    force_default_wallpaper = 0 # Set to 0 or 1 to disable the anime mascot wallpapers
    disable_hyprland_logo = true # If true disables the random hyprland logo / anime girl background. :(
}
#+end_src

* Keyboard
Oh boy, things get hypr here :)

** Input Management
Keyboard layout, etc.
#+begin_src conf
input {
    kb_layout = us
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    follow_mouse = 1

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.

    touchpad {
        natural_scroll = false
    }
}

gestures {
    workspace_swipe = false
}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#per-device-input-configs for more
device {
    name = epic-mouse-v1
    sensitivity = -0.5
}
#+end_src

** Keybindings
Wonder why this is a subheading, I will add a table of keybinds soon, like Qtile.
#+begin_src conf
# See https://wiki.hyprland.org/Configuring/Keywords/
$mainMod = SUPER # Sets "Windows" key as main modifier

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, RETURN, exec, $terminal
bind = $mainMod, Q, killactive,
bind = $mainMod, L, exec, systemctl suspend
bind = $mainMod SHIFT, Q, exit,
bind = $mainMod, E, exec, emacsclient --create-frame
bind = $mainMod, V, exec, zen-browser
bind = $mainMod SHIFT, SPACE, togglefloating,
bind = $mainMod, D, exec, $menu
bind = $mainMod, R, exec, $menur
bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, J, togglesplit, # dwindle

# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

# Example special workspace (scratchpad)
bind = $mainMod, S, togglespecialworkspace, magic
bind = $mainMod SHIFT, S, movetoworkspace, special:magic

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

bind = $mainMod, E, exec, emacsclient --create-frame
# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

# Laptop multimedia keys for volume and LCD brightness
bindel = ,XF86AudioRaiseVolume, exec, wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%+
bindel = ,XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
bindel = ,XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
bindel = ,XF86AudioMicMute, exec, wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle
bindel = ,XF86MonBrightnessUp, exec, brightnessctl s 10%+
bindel = ,XF86MonBrightnessDown, exec, brightnessctl s 10%-

# Requires playerctl
bindl = , XF86AudioNext, exec, playerctl next
bindl = , XF86AudioPause, exec, playerctl play-pause
bindl = , XF86AudioPlay, exec, playerctl play-pause
bindl = , XF86AudioPrev, exec, playerctl previous
#+end_src

* Windows & Workspaces
Just some defaults I'm not touching at the moment.
#+begin_src conf
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
# See https://wiki.hyprland.org/Configuring/Workspace-Rules/ for workspace rules

# Example windowrule v1
# windowrule = float, ^(kitty)$

# Example windowrule v2
# windowrulev2 = float,class:^(kitty)$,title:^(kitty)$

# Ignore maximize requests from apps. You'll probably like this.
windowrulev2 = suppressevent maximize, class:.*

# Fix some dragging issues with XWayland
windowrulev2 = nofocus,class:^$,title:^$,xwayland:1,floating:1,fullscreen:0,pinned:0
#+end_src
