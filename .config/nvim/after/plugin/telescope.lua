local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>.', builtin.find_files, { desc = 'Telescope find files' })
