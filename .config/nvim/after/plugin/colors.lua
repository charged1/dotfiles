-- KEEP BACKGROUND TO BE TRANSPARENT

function ColorMyPencils(color)
	color = color or "onedark"
	vim.cmd.colorscheme(color)

	vim.api.nvim_set_hl(0, "Normal", { bg = "none"})
	vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none"})
end

ColorMyPencils()
	vim.api.nvim_set_hl(0, "Normal", { bg = "none"})
