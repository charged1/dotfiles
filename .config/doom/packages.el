;; doom sync or 'M-x doom/reload' after adding
;(package! some-package)
;; directly from git repo
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; disable package that built into doom
;(package! builtin-package :disable t)

;; override built in package recipies
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; specify branch
;;(package! builtin-package :recipe (:branch "develop"))

(package! org-auto-tangle)
(package! org-superstar)
(package! neotree)
(package! flycheck-aspell)
(package! calfw)
(package! calfw-org)
(package! crdt)
(package! dashboard)
(package! dired-open)
(package! dired-subtree)
(package! dmenu)
(package! elfeed-goodies)
(package! emojify)
(package! evil-tutor)
(package! exwm)
(package! ivy-posframe)
(package! mw-thesaurus)
(package! org-auto-tangle)
(package! org-web-tools)
;; (package! ox-gemini)
(package! peep-dired)
(package! password-store)
(package! rainbow-mode)
(package! resize-window)
(package! tldr)
(package! beacon)
(package! clippy)
(package! minimap)
(package! olivetti)
(package! erc)
(package! quelpa)
(package! quelpa-use-package)
(package! wc-mode)
(package! good-scroll)
