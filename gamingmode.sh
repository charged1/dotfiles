#!/bin/bash

# Just some commands to run and reverse to improve gaming performance

if [[ "$1" == "off" ]]; then
    emacs --daemon &
    conky -c /home/sougato/.config/conky/config.conf &
    picom &
    # we'll choose not to start chrome back up
else
    killall emacs
    killall emacs-gtk
    killall conky
    killall picom
fi
